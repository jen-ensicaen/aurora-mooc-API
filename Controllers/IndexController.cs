using System;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace AuroraAPI.Controllers
{
    [Route("/")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class IndexController : Controller
    {
        [HttpGet]
        public string Get()
        {
            return "Jello World";
        }
    }
}
