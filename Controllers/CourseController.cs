using System;
using System.Collections.Generic;
using AuroraAPI.Data;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace AuroraAPI.Controllers
{
    /// <summary>
    /// The endpoint that manages courses.
    /// </summary>
    [Route("api/course")]
    public class CourseController : Controller
    {
        private MoocContext _context;

        public CourseController(MoocContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves the course with the specified id.
        /// </summary>
        /// <param name="id">course id</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Course), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public IActionResult Get(int id)
        {
            var course = _context.Courses
                .Include(c => c.Creator)
                .Include(c => c.Category)
                .Include(c => c.MainMedia)
                .Include(c => c.Difficulty)
                .Include(c => c.Comments)
                    .ThenInclude(c => c.Children)
                .SingleOrDefault(c => c.Id == id);
            
            if (course == null)
            {
                return NotFound();
            }

            return Ok(course);
        }

        /// <summary>
        /// Creates a new course.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Course), 200)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR_AND_CREATOR)]
        public IActionResult Create([FromBody]CreateCourseRequestParams courseParams)
        {
            var user = GetLoggedInUser();

            if (user == null)
            {
                return Unauthorized();
            }

            var course = new Course
            {
                Creator = user,
                Title = courseParams.Title,
                Description = courseParams.Description,
                Content = courseParams.Content,
                Difficulty = _context.CourseDifficulties
                                .Where(d => d.Id == courseParams.DifficultyId)
                                .First(),
                Category = _context.CourseCategories
                                .Where(c => c.Id == courseParams.CategoryId)
                                .First()
            };

            if (courseParams.MainMediaId != null)
            {
                course.MainMedia = new Media { Id = courseParams.MainMediaId.Value };
                _context.Entry(course.MainMedia).State = EntityState.Detached;
            }
 
            _context.Add(course);

            _context.Entry(course.Creator).State = EntityState.Detached;
            _context.Entry(course.Category).State = EntityState.Detached;

            _context.SaveChanges();

            return Ok(course);
        }

        /// <summary>
        /// Updates the content and metadata of a course.
        /// </summary>
        [HttpPut]
        [ProducesResponseType(typeof(Course), 200)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 404)]
        [Authorize(Roles = UserType.ADMINISTRATOR_AND_CREATOR)]
        public IActionResult Update([FromBody]UpdateCourseRequestParams courseParams)
        {
            var course = _context.Courses
                .Where(c => c.Id == courseParams.Id)
                .FirstOrDefault();

            if (course == null)
            {
                return NotFound();
            }

            course.Content = courseParams.Content;
            course.Description = courseParams.Description;
            course.Title = courseParams.Title;
            course.Difficulty = _context.CourseDifficulties
                                .Where(d => d.Id == courseParams.DifficultyId)
                                .First();
            course.Category = _context.CourseCategories
                                .Where(c => c.Id == courseParams.CategoryId)
                                .First();

            if (courseParams.MainMediaId != null)
            {
                course.MainMedia = new Media { Id = courseParams.MainMediaId.Value };
                _context.Entry(course.MainMedia).State = EntityState.Detached;
            }

            _context.SaveChanges();

            return Ok(course);
        }

        /// <summary>
        /// Retrieves the list of categories.
        /// </summary>
        [HttpGet("category")]
        [ProducesResponseType(typeof(IList<CourseCategory>), 200)]
        public IActionResult GetCategories()
        {
            var categories = _context.CourseCategories
                .Include(c => c.Children)
                .ToList()
                .Where(c => !c.CourseCategoryId.HasValue)
                .ToList();

            return Ok(categories);
        }

        /// <summary>
        /// Retrieves all the courses in a category.
        /// </summary>
        /// <param name="id">category id</param>
        [HttpGet("category/{id}")]
        [ProducesResponseType(typeof(IList<Course>), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public IActionResult GetByCategory(int id)
        {
            if (!_context.CourseCategories.Any(c => c.Id == id))
            {
                return NotFound();
            }

            var courses = _context.Courses
                .Include(c => c.Creator)
                .Include(c => c.Category)
                .Where(c => c.Category.Id == id)
                .ToList();

            return Ok(courses);
        }

        /// <summary>
        /// Creates a new course category.
        /// </summary>
        [HttpPost("category")]
        [ProducesResponseType(typeof(CourseCategory), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR)]
        public IActionResult CreateCourseCategory([FromBody]CreateCourseCategoryRequestParams categoryParams)
        {
            var category = new CourseCategory
            {
                Label = categoryParams.Label
            };

            _context.Add(category);
            _context.SaveChanges();

            // Is this new category a child of an existing category?
            if (categoryParams.ParentCategoryId.HasValue)
            {
                var parent = _context.CourseCategories
                    .Where(c => c.Id == categoryParams.ParentCategoryId)
                    .Include(c => c.Children)
                    .FirstOrDefault();
                
                if (parent == null)
                {
                    return BadRequest("Invalid parent category id");
                }

                parent.Children.Add(category);
            }

            _context.SaveChanges();
            return Ok(category);
        }

        /// <summary>
        /// Creates a new course difficulty.
        /// </summary>
        [HttpPost("difficulty")]
        [ProducesResponseType(typeof(CourseDifficulty), 200)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR)]
        public IActionResult CreateCourseDifficulty([FromBody]CreateCourseDifficultyRequestParams difficultyParams)
        {
            var difficulty = new CourseDifficulty
            {
                Label = difficultyParams.Label
            };

            _context.Add(difficulty);
            _context.SaveChanges();
            return Ok(difficulty);
        }

        /// <summary>
        /// Posts a comment on a course.
        /// </summary>
        /// <param name="courseId">the course under which to comment</param>
        [HttpPost("{courseId}/comment")]
        [ProducesResponseType(typeof(Comment), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 404)]
        [Authorize]
        public IActionResult CreateComment(int courseId, [FromBody]CreateCommentRequestParams commentParams)
        {
            // Get current logged in user
            var user = GetLoggedInUser();

            if (user == null)
            {
                return Unauthorized();
            }

            // Get parent course
            var course = _context.Courses
                .Where(c => c.Id == courseId)
                .Include(c => c.Comments)
                .FirstOrDefault();

            if (course == null)
            {
                return BadRequest("Course id is invalid");
            }

            // Create comment and add it to context
            var comment = new Comment
            {
                User = user,
                Message = commentParams.Message
            };

            course.Comments.Add(comment);

            // If this comment is replying to another comment, save that
            if (commentParams.ParentId.HasValue)
            {
                var parent = _context.Comments
                    .Where(c => c.CourseId == courseId)
                    .Where(c => c.Id == commentParams.ParentId)
                    .FirstOrDefault();

                if (parent == null)
                {
                    return BadRequest("Parent comment is invalid");
                }

                parent.Children.Add(comment);
            }

            _context.SaveChanges();

            return Ok(comment);
        }

        private User GetLoggedInUser()
        {
            var userClaims = User.Claims;
            var user = _context.Users
                .Where(u => u.Username == userClaims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .FirstOrDefault()
                    .Value
                )
                .Include(u => u.Type)
                .FirstOrDefault();
            
            return user;
        }
    }

    public class CreateCourseRequestParams
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public int DifficultyId { get; set; }

        public int? MainMediaId { get; set; }

        public int CategoryId { get; set; }
    }

    public class UpdateCourseRequestParams
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }

        public int DifficultyId { get; set; }

        public int? MainMediaId { get; set; }

        public int CategoryId { get; set; }
    }

    public class CreateCourseCategoryRequestParams
    {
        public string Label { get; set; }

        public int? ParentCategoryId { get; set; }
    }

    public class CreateCourseDifficultyRequestParams
    {
        public string Label { get; set; }
    }

    public class CreateCommentRequestParams
    {
        public string Message { get; set; }

        public int? ParentId { get; set; }
    }
}
