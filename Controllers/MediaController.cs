using System;
using AuroraAPI.Data;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Http;
using ByteSizeLib;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace AuroraAPI.Controllers
{
    /// <summary>
    /// The endpoint that manages media, and media upload (audio, video, images).
    /// </summary>
    [Route("api/media")]
    public class MediaController : Controller
    {
        private MoocContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        private static Dictionary<string, string> ImageTypeExtensions = new Dictionary<string, string>() {
            { "image/gif", "gif" },
            { "image/jpeg", "jpeg" },
            { "image/png", "png" },
            { "image/svg+xml", "svg"}
        };

        private static Dictionary<string, string> AudioTypeExtensions = new Dictionary<string, string>() {
            { "audio/mpeg", "mp3" },
            { "audio/mpeg3", "mp3" },
            { "audio/x-mpeg-3", "mp3" },
            { "audio/aac", "aac"},
            { "audio/ogg", "ogg" },
            { "audio/webm", "webm" },
            { "audio/wave", "wav" },
            { "audio/wav", "wav" },
            { "audio/x-wav", "wav" },
            { "audio/x-pn-wav", "wav" },
        };

        private static Dictionary<string, string> VideoTypeExtensions = new Dictionary<string, string>() {
            { "video/mp4", "mp4" },
            { "video/ogg", "ogv" },
            { "application/ogg", "ogv" },
            { "video/webm", "webm" }
        };

        public MediaController(MoocContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Retrieves information about the media with the specified id.
        /// </summary>
        /// <param name="id">media id</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Media), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public IActionResult Get(int id)
        {
            var media = _context.Media
                .Where(m => m.Id == id)
                .Include(m => m.Type)
                .FirstOrDefault();

            if (media == null)
            {
                return NotFound();
            }

            return Ok(media);
        }

        /// <summary>
        /// Creates and uploads a new audio resource.
        /// Supported formats: mp3, aac, ogg, webm, wav
        /// </summary>
        /// <param name="media">information about the uploaded media</param>
        [HttpPost("audio")]
        [ProducesResponseType(typeof(Media), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR_AND_CREATOR)]
        public async Task<IActionResult> UploadAudio([FromForm]UploadMediaRequestParams mediaParams, [FromForm] IFormFile file)
        {
            return await SaveMedia(new MediaType { Id = "AUDIO" }, mediaParams, file, AudioTypeExtensions);
        }

        /// <summary>
        /// Creates and uploads a new video resource.
        /// Supported formats: mp4, ogv, webm
        /// </summary>
        /// <param name="media">information about the uploaded media</param>
        [HttpPost("video")]
        [ProducesResponseType(typeof(Media), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR_AND_CREATOR)]
        public async Task<IActionResult> UploadVideo([FromForm]UploadMediaRequestParams mediaParams, [FromForm] IFormFile file)
        {
            return await SaveMedia(new MediaType { Id = "VIDEO" }, mediaParams, file, VideoTypeExtensions);
        }

        /// <summary>
        /// Creates and uploads a new image resource.
        /// Supported formats: gif, jpeg, png, svg
        /// </summary>
        /// <param name="media">information about the uploaded media</param>
        [HttpPost("image")]
        [ProducesResponseType(typeof(Media), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [Authorize(Roles = UserType.ADMINISTRATOR_AND_CREATOR)]
        public async Task<IActionResult> UploadImage([FromForm]UploadMediaRequestParams mediaParams, [FromForm] IFormFile file)
        {
            return await SaveMedia(new MediaType { Id = "IMAGE" }, mediaParams, file, ImageTypeExtensions);
        }

        private async Task<IActionResult> SaveMedia(MediaType mediaType, UploadMediaRequestParams mediaParams, IFormFile file, Dictionary<string, string> extensions)
        {
            var media = new Media
            {
                Title = mediaParams.Title,
                Description = mediaParams.Description,
                Type = mediaType
            };

            _context.Add(media);
            _context.Entry(media.Type).State = EntityState.Detached;

            if (file.Length > ByteSize.FromMegaBytes(100).Bytes)
            {
                return BadRequest("The file size must not exceed 100 MB");
            }

            if (file.Length <= 0)
            {
                return BadRequest("The file is empty");
            }

            // Check if we're uploaded an audio file in a supported format
            // First check mime type
            string finalExtension;
            if (extensions.ContainsKey(file.ContentType))
            {
                finalExtension = extensions[file.ContentType];
            }
            else
            {
                // If mime type is bad, check extension anyway
                var ext = Path.GetExtension(file.FileName);
                if (!extensions.Values.Any(x => "." + x == ext))
                {
                    // If both mime and extension are bad, welp we can't do much
                    return BadRequest("The file is not in a supported format");
                }

                finalExtension = ext.Substring(1);
            }

            media.Filename = Guid.NewGuid().ToString() + "." + finalExtension;
            
            var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, "media", mediaType.Id.ToLowerInvariant(), media.Filename);
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            _context.SaveChanges();
            return Ok(media);
        }
    }

    public class UploadMediaRequestParams
    {
        public string Title { get; set; }
        
        public string Description { get; set; }
    }
}
