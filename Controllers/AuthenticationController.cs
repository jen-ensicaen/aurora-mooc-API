using System;
using AuroraAPI.Data;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.Extensions.Options;
using AuroraAPI.Authentication;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.Security.Principal;

namespace AuroraAPI.Controllers
{
    /// <summary>
    /// The endpoint that manages user authentication and token generation.
    /// </summary>
    [Route("api/token")]
    public class AuthenticationController : Controller
    {
        private MoocContext _context;
        private TokenProviderOptions _options;

        public AuthenticationController(MoocContext context, IOptions<TokenProviderOptions> options)
        {
            _context = context;
            _options = options.Value;
            ThrowIfInvalidOptions(_options);
        }

        /// <summary>
        /// Authenticates the user and gives them a token.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Token), 200)]
        [ProducesResponseType(typeof(void), 400)]
        public async Task<IActionResult> GenerateToken([FromForm]GenerateTokenRequestParams credentials)
        {
            var identity = await GetIdentity(credentials.Username, credentials.Password);
            if (identity == null)
            {
                return BadRequest("Invalid username or password.");
            }

            var now = DateTime.UtcNow;

            var role = identity.Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .FirstOrDefault();

            // Specifically add the jti (nonce), iat (issued timestamp), and sub (subject/user) claims.
            // You can add other claims here, if you want:
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, credentials.Username),
                new Claim(JwtRegisteredClaimNames.Jti, await _options.NonceGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.Role, role.Value),
                new Claim(ClaimTypes.NameIdentifier, credentials.Username)
            };

            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(_options.Expiration),
                signingCredentials: _options.SigningCredentials);

            var user = _context.Users
                .Where(u => u.Username == credentials.Username)
                .First();

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new Token
            {
                access_token = encodedJwt,
                expires_in = (int)_options.Expiration.TotalSeconds,
                user = user
            };

            return Ok(response);
        }

        private Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var user = _context.Users
                .Include(u => u.Type)
                .Where(u => AuthenticateUser(u, username, password))
                .FirstOrDefault();

            if (user != null)
            {
                return Task.FromResult(new ClaimsIdentity(new GenericIdentity(username, "Token"), new Claim[] {
                    new Claim(ClaimTypes.Role, user.Type.Id)
                }));
            }

            // Credentials are invalid, or account doesn't exist
            return Task.FromResult<ClaimsIdentity>(null);
        }

        private bool AuthenticateUser(User user, string username, string password)
        {
            var sha = SHA256.Create();

            if (user.Username == username || user.Email == username)
            {
                var hash = sha.ComputeHash(Encoding.ASCII.GetBytes(password));
                if (Enumerable.SequenceEqual(user.PasswordHash, hash))
                {
                    return true;
                }
            }

            return false;
        }

        private static void ThrowIfInvalidOptions(TokenProviderOptions options)
        {
            if (string.IsNullOrEmpty(options.Issuer))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Issuer));
            }

            if (string.IsNullOrEmpty(options.Audience))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Audience));
            }

            if (options.Expiration == TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenProviderOptions.Expiration));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.SigningCredentials));
            }

            if (options.NonceGenerator == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.NonceGenerator));
            }
        }

        /// <summary>
        /// Get this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        public static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }

    public class GenerateTokenRequestParams
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
