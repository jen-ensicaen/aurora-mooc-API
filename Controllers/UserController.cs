using System;
using System.Linq;
using AuroraAPI.Data;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace AuroraAPI.Controllers
{
    /// <summary>
    /// Endpoint that manages user info.
    /// </summary>
    [Route("api/user")]
    public class UserController : Controller
    {
        private MoocContext _context;
        
        public UserController(MoocContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retreives the user with the specified ID.
        /// </summary>
        /// <param name="id">user id</param>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public IActionResult Get(int id)
        {
            var user = _context.Users
                .Where(u => u.Id == id)
                .Include(u => u.Type)
                .FirstOrDefault();
            
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// Retreives the user with the specified username.
        /// </summary>
        /// <param name="username">the username</param>
        [HttpGet("byUsername/{username}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(void), 404)]
        public IActionResult GetByUsername(string username)
        {
            var user = _context.Users
                .Where(u => u.Username == username)
                .Include(u => u.Type)
                .FirstOrDefault();
            
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// Creates a new user.
        /// Min password length: 10 chars.
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(User), 400)]
        public IActionResult Create([FromBody]CreateUserRequestParams userParams)
        {
            if (string.IsNullOrWhiteSpace(userParams.Password) || userParams.Password.Length < 10)
            {
                return BadRequest("Password must be at least a little bit decent");
            }

            var user = new User
            {
                FirstName = userParams.FirstName,
                LastName = userParams.LastName,
                Username = userParams.Username,
                Email = userParams.Email,
                Type = new UserType { Id = UserType.BASIC },
                ClearTextPassword = userParams.Password
            };

            // Always create normal users by default
            user.Type = new UserType { Id = UserType.BASIC };

            _context.Add(user);
            _context.Entry(user.Type).State = EntityState.Detached;
            _context.SaveChanges();

            return Ok(user);
        }

        /// <summary>
        /// Updates an existing user's info and password (if required).
        /// </summary>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        public IActionResult Update([FromBody]UpdateUserRequestParams userParams)
        {
            var user = GetLoggedInUser();

            if (user == null)
            {
                return Unauthorized();
            }

            if (!string.IsNullOrWhiteSpace(userParams.NewPassword))
            {
                // We're trying to change the password
                // Check the old password
                var sha = SHA256.Create();
                var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(userParams.OldPassword));

                if (!Enumerable.SequenceEqual(user.PasswordHash, hash))
                {
                    return BadRequest("Invalid old password");
                }

                if (userParams.NewPassword.Length < 10)
                {
                    return BadRequest("Password must be at least 10 characters long");
                }

                // Everything is okay, update the password
                user.ClearTextPassword = userParams.NewPassword;
            }

            user.Email = userParams.Email;
            user.LastName = userParams.LastName;
            user.FirstName = userParams.FirstName;

            _context.SaveChanges();

            return Ok(user);
        }

        private User GetLoggedInUser()
        {
            var userClaims = User.Claims;
            var user = _context.Users
                .Where(u => u.Username == userClaims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .FirstOrDefault()
                    .Value
                )
                .FirstOrDefault();
            
            return user;
        }
    }

    public class CreateUserRequestParams
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }

    public class UpdateUserRequestParams
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Email { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
