using System;
using System.Linq;
using AuroraAPI.Data;
using AuroraAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace AuroraAPI.Controllers
{
    /// <summary>
    /// Endpoint that manages comments.
    /// </summary>
    [Route("api/comment")]
    public class CommentController : Controller
    {
        private MoocContext _context;
        
        public CommentController(MoocContext context)
        {
            _context = context;
        }

        private User GetLoggedInUser()
        {
            var userClaims = User.Claims;
            var user = _context.Users
                .Where(u => u.Username == userClaims
                    .Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .FirstOrDefault()
                    .Value
                )
                .FirstOrDefault();
            
            return user;
        }
    }
}
