using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuroraAPI.Models
{
    public class Course
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        public string Content { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreationDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? LastModifiedDate { get; set; }

        [Required]
        public CourseDifficulty Difficulty { get; set; }

        public Media MainMedia { get; set; }

        [Required]
        public User Creator { get; set; }

        [Required]
        public CourseCategory Category { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}