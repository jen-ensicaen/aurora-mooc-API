using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace AuroraAPI.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        public string Message { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Date { get; set; }

        public ICollection<Comment> Children { get; set; }
        
        [Required]
        public User User { get; set; }

        [JsonIgnore]
        public int? CommentId { get; set; }

        [JsonIgnore]
        public int CourseId { get; set; }
    }
}