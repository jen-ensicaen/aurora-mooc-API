using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace AuroraAPI.Models
{
    public class CourseCategory
    {
        public int Id { get; set; }
        
        [Required]
        public string Label { get; set; }
        
        public ICollection<CourseCategory> Children { get; set; }

        [JsonIgnore]
        public int? CourseCategoryId { get; set; }
    }
}