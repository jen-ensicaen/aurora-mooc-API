using System;
using System.ComponentModel.DataAnnotations;

namespace AuroraAPI.Models
{
    public class CourseDifficulty
    {
        public int Id { get; set; }

        [Required]
        public string Label { get; set; }
    }
}