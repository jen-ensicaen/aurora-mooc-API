using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuroraAPI.Models
{
    public class MediaType
    {
        public const string VIDEO = "VIDEO";
        public const string AUDIO = "AUDIO";
        public const string IMAGE = "IMAGE";        

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Required]
        public string Label { get; set; }
    }
}