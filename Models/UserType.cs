using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuroraAPI.Models
{
    public class UserType
    {
        public const string ADMINISTRATOR = "ADMINISTRATOR";
        public const string CREATOR = "CREATOR";
        public const string BASIC = "BASIC";

        public const string ADMINISTRATOR_AND_CREATOR = ADMINISTRATOR + "," + CREATOR;

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Required]
        public string Label { get; set; }
    }
}