using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace AuroraAPI.Models
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Email { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreationDate { get; set; }

        [Required]
        public UserType Type { get; set; }

        [JsonIgnore]
        public byte[] PasswordHash { get; set; } 

        [NotMapped]
        public string ClearTextPassword
        { 
            set
            {
                var sha = SHA256.Create();
                PasswordHash = sha.ComputeHash(Encoding.UTF8.GetBytes(value));
            }
        }

        [NotMapped]
        public string ProfilePictureUrl
        {
            get
            {
                var hash = GetGravatarHash();
                return $"https://www.gravatar.com/avatar/{hash}";
            }
        }

        private string GetGravatarHash()
        {
            var md5 = MD5.Create();
            var emailHash = md5.ComputeHash(Encoding.UTF8.GetBytes(Email.ToLowerInvariant()));
            return string.Concat(emailHash.Select(b => b.ToString("x2")).ToArray());
        }
    }
}