using System;
using System.IO;
using System.Text;
using AuroraAPI.Authentication;
using AuroraAPI.Data;
using AuroraAPI.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using MySQL.Data.Entity.Extensions;
using Swashbuckle.AspNetCore.Swagger;

namespace AuroraAPI
{
    public partial class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddDbContext<MoocContext>(options =>
                options.UseMySQL(Configuration.GetConnectionString("DefaultConnection")));

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info 
                    {
                        Title = "Aurora API",
                        Description = "This is a complete API that allows access to the complete MOOC database of the Aurora project.",
                        Version = "v1"
                    });

                var docPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "AuroraAPI.xml");
                c.IncludeXmlComments(docPath);
                
                c.OperationFilter<AddAuthTokenHeaderOperationFilter>();
                c.OperationFilter<FileUploadOperationFilter>();
            });

            var signingKey = new SymmetricSecurityKey(
                Encoding.ASCII.GetBytes(Configuration["Secrets:TokenSecret"]));

            services.AddOptions();

            services.Configure<TokenProviderOptions>(c =>
            {
                c.Audience = "aurora-users";
                c.Issuer = "project-aurora";
                c.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
                c.Expiration = TimeSpan.FromDays(7);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, MoocContext context)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            DatabaseInitializer.Initialize(context);

            var signingKey = new SymmetricSecurityKey(
                Encoding.ASCII.GetBytes(Configuration["Secrets:TokenSecret"]));

            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
            
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = "project-aurora",
            
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = "aurora-users",
            
                // Validate the token expiry
                ValidateLifetime = true,
            
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };
            
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });

            app.UseMvc();

            app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Aurora API v1");
            });
        }
    }
}
