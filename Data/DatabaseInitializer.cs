using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using AuroraAPI.Models;

namespace AuroraAPI.Data
{
    public class DatabaseInitializer
    {
        public static void Initialize(MoocContext context)
        {
            context.Database.EnsureCreated();

            // Detect if the database is already seeded
            if (context.UserTypes.Any())
            {
                return;
            }

            var difficulties = new CourseDifficulty[]
            {
                new CourseDifficulty { Label = "Grand débutant" },
                new CourseDifficulty { Label = "Débutant" },
                new CourseDifficulty { Label = "Intermédiaire" },
                new CourseDifficulty { Label = "Avancé" },
                new CourseDifficulty { Label = "Expert" },
            };

            foreach (var d in difficulties)
            {
                context.CourseDifficulties.Add(d);
            }

            context.SaveChanges();

            var categories = new CourseCategory[]
            {
                new CourseCategory { Label = "Droit et juridique" },
                new CourseCategory { Label = "Histoire et géographie", Children = new Collection<CourseCategory> {
                    new CourseCategory { Label = "Histoire" },
                    new CourseCategory { Label = "Géographie" }
                } },
                new CourseCategory { Label = "Musique" },
                new CourseCategory { Label = "Sport", Children = new Collection<CourseCategory> {
                    new CourseCategory { Label = "Handball" },
                    new CourseCategory { Label = "Football" }
                } },
                new CourseCategory { Label = "Cuisine", Children = new Collection<CourseCategory> {
                    new CourseCategory { Label = "Entrées" },
                    new CourseCategory { Label = "Plats" },
                    new CourseCategory { Label = "Desserts" }
                } },
                new CourseCategory { Label = "Bricolage" },
                new CourseCategory { Label = "Jardinage" },
                new CourseCategory { Label = "Santé" },
                new CourseCategory { Label = "Informatique", Children = new Collection<CourseCategory> {
                    new CourseCategory { Label = "Réseaux et télécommunications" },
                    new CourseCategory { Label = "Programmation et développement" }
                } }
            };

            foreach (var c in categories)
            {
                context.CourseCategories.Add(c);
            }

            context.SaveChanges();

            var mediaTypes = new MediaType[]
            {
                new MediaType { Id = MediaType.AUDIO, Label = "Audio" },
                new MediaType { Id = MediaType.VIDEO, Label = "Vidéo" },
                new MediaType { Id = MediaType.IMAGE, Label = "Image" },
            };

            foreach (var mt in mediaTypes)
            {
                context.MediaTypes.Add(mt);
            }

            context.SaveChanges();

            var userTypes = new UserType[]
            {
                new UserType { Id = UserType.ADMINISTRATOR, Label = "Administrateur" },
                new UserType { Id = UserType.CREATOR, Label = "Créateur" },
                new UserType { Id = UserType.BASIC, Label = "Utilisateur" },
            };

            foreach (var ut in userTypes)
            {
                context.UserTypes.Add(ut);
            }

            context.SaveChanges();

            var users = new User[]
            {
                new User 
                {
                    FirstName = "Baptiste", 
                    LastName = "Candellier", 
                    Email = "baptiste.candellier@ecole.ensicaen.fr", 
                    Username = "candellier",
                    Type = context.UserTypes.Where(ut => ut.Id == UserType.ADMINISTRATOR).First(),
                    ClearTextPassword = "candellier"
                },

                new User 
                {
                    FirstName = "Jennifer",
                    LastName = "Gränicher", 
                    Email = "jennifer.granicher@ecole.ensicaen.fr",
                    Username = "m1ssc0k4",
                    Type = context.UserTypes.Where(ut => ut.Id == UserType.ADMINISTRATOR).First(),
                    ClearTextPassword = "m1ssc0k4"
                },

                new User 
                {
                    FirstName = "Pénélope",
                    LastName = "Girod", 
                    Email = "penelope.girod@ecole.ensicaen.fr",
                    Username = "pika",
                    Type = context.UserTypes.Where(ut => ut.Id == UserType.ADMINISTRATOR).First(),
                    ClearTextPassword = "pika"
                }
            };

            foreach (var user in users)
            {
                context.Users.Add(user);
            }

            context.SaveChanges();

            var media = new Media[]
            {
                new Media 
                {
                    Description = "Les pâtes au thon, un plat idéal quand on n'a pas le temps de cuisiner et quand on doit faire avec les moyens du bord ! Une recette express et toujours appréciée.",
                    Filename = "recette_pates_au_thon.mp4",
                    Title = "Recette des Pâtes au thon",
                    Type = context.MediaTypes.Where(mt => mt.Id == MediaType.VIDEO).First()
                }
            };

            foreach (var medium in media)
            {
                context.Media.Add(medium);
            }

            context.SaveChanges();

            var courses = new Course[]
            {
                new Course 
                {
                    Content = "<p><strong>Temps de préparation :</strong> 10 minutes<br><strong>Temps de cuisson :</strong> 10 minutes</p><h4>Ingrédients (pour 2 personnes) :</h4><ul><li>400 à 600 g de pâtes (suivant l'appétit)</li><li>thon en boîte émietté</li><li>1 oignon en lamelles</li><li>1 petite boîte de concentré de tomates</li><li>1 verre de crème fraîche légère liquide</li><li>gruyère râpé</li><li>sel et poivre</li></ul><h4>Préparation de la recette :</h4><p>Faire cuire les pâtes selon votre goût et le type de pâtes.<br>Dans une casserole, faire revenir les oignons avec un peu d'huile, ajouter le thon, le concentré de tomates, la crème, le sel et le poivre.<br>Egoutter les pâtes, et les mélanger avec la préparation.<br>Ajouter le gruyère râpé.</p>",
                    Description = "Un plat idéal quand on n'a pas le temps de cuisiner et quand on doit faire avec les moyens du bord !",
                    Title = "Recette des Pâtes au thon",
                    Creator = context.Users.Where(u => u.Username == "candellier").First(),
                    Difficulty = context.CourseDifficulties.Where(d => d.Label == "Grand débutant").First(),
                    Category = context.CourseCategories.Where(c => c.Label == "Plats").First()
                }
            };

            foreach (var course in courses)
            {
                context.Courses.Add(course);
            }

            context.SaveChanges();
        }
    }
}
