using System;
using AuroraAPI.Models;
using Microsoft.EntityFrameworkCore;
using MySQL.Data.Entity.Extensions;

namespace AuroraAPI.Data
{
    public class MoocContext : DbContext
    {
        public MoocContext(DbContextOptions<MoocContext> options) : base(options)
        {
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseCategory> CourseCategories { get; set; }
        public DbSet<CourseDifficulty> CourseDifficulties { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Media> Media { get; set; }
        public DbSet<MediaType> MediaTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(u => u.CreationDate)
                .HasDefaultValueSql("now()");

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique(true);

            modelBuilder.Entity<Course>()
                .Property(c => c.CreationDate)
                .HasDefaultValueSql("now()");

            modelBuilder.Entity<Comment>()
                .Property(c => c.Date)
                .HasDefaultValueSql("now()");

            modelBuilder.Entity<Course>()
                .Property(c => c.Content)
                .HasColumnType("text");
            
            modelBuilder.Entity<Comment>()
                .Property(c => c.Message)
                .HasColumnType("text");
        }
    }
}