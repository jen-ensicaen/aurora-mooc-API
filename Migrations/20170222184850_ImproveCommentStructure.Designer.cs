﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AuroraAPI.Data;

namespace AuroraAPI.Migrations
{
    [DbContext(typeof(MoocContext))]
    [Migration("20170222184850_ImproveCommentStructure")]
    partial class ImproveCommentStructure
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("AuroraAPI.Models.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CommentId");

                    b.Property<int>("CourseId");

                    b.Property<DateTime?>("Date")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("Message")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CommentId");

                    b.HasIndex("CourseId");

                    b.HasIndex("UserId");

                    b.ToTable("Comment");
                });

            modelBuilder.Entity("AuroraAPI.Models.Course", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime?>("CreationDate")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<int>("CreatorId");

                    b.Property<string>("Description");

                    b.Property<int>("DifficultyId");

                    b.Property<DateTime?>("LastModifiedDate")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<int?>("MainMediaId");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CreatorId");

                    b.HasIndex("DifficultyId");

                    b.HasIndex("MainMediaId");

                    b.ToTable("Course");
                });

            modelBuilder.Entity("AuroraAPI.Models.CourseCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CourseCategoryId");

                    b.Property<string>("Label")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CourseCategoryId");

                    b.ToTable("CourseCategory");
                });

            modelBuilder.Entity("AuroraAPI.Models.CourseDifficulty", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Label")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("CourseDifficulty");
                });

            modelBuilder.Entity("AuroraAPI.Models.Media", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Filename")
                        .IsRequired();

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<string>("TypeId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("TypeId");

                    b.ToTable("Media");
                });

            modelBuilder.Entity("AuroraAPI.Models.MediaType", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Label")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("MediaType");
                });

            modelBuilder.Entity("AuroraAPI.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreationDate")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<byte[]>("PasswordHash");

                    b.Property<string>("TypeId")
                        .IsRequired();

                    b.Property<string>("Username")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.HasIndex("TypeId");

                    b.HasIndex("Username")
                        .IsUnique();

                    b.ToTable("User");
                });

            modelBuilder.Entity("AuroraAPI.Models.UserType", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Label")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("UserType");
                });

            modelBuilder.Entity("AuroraAPI.Models.Comment", b =>
                {
                    b.HasOne("AuroraAPI.Models.Comment")
                        .WithMany("Children")
                        .HasForeignKey("CommentId");

                    b.HasOne("AuroraAPI.Models.Course")
                        .WithMany("Comments")
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("AuroraAPI.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("AuroraAPI.Models.Course", b =>
                {
                    b.HasOne("AuroraAPI.Models.CourseCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("AuroraAPI.Models.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("AuroraAPI.Models.CourseDifficulty", "Difficulty")
                        .WithMany()
                        .HasForeignKey("DifficultyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("AuroraAPI.Models.Media", "MainMedia")
                        .WithMany()
                        .HasForeignKey("MainMediaId");
                });

            modelBuilder.Entity("AuroraAPI.Models.CourseCategory", b =>
                {
                    b.HasOne("AuroraAPI.Models.CourseCategory")
                        .WithMany("Children")
                        .HasForeignKey("CourseCategoryId");
                });

            modelBuilder.Entity("AuroraAPI.Models.Media", b =>
                {
                    b.HasOne("AuroraAPI.Models.MediaType", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("AuroraAPI.Models.User", b =>
                {
                    b.HasOne("AuroraAPI.Models.UserType", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
