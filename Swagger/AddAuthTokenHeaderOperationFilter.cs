using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AuroraAPI.Swagger
{
    public class AddAuthTokenHeaderOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var authAttributes = context.ApiDescription
                .ControllerAttributes()
                .Union(context.ApiDescription.ActionAttributes())
                .OfType<AuthorizeAttribute>();

            // If this is a restricted route, ask for an authorization header
            if (authAttributes.Any())
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<IParameter>();
                
                operation.Parameters.Add(new HeaderParameter()
                {
                    Name = "Authorization",
                    In = "header",
                    Type = "string",
                    Required = false,
                    Default = "Bearer "
                });
            }
        }
    }

    class HeaderParameter : NonBodyParameter
    {
    }
}